# Owi : OpenBSD Wifi Manager

[La page du projet est maintenant sur le site du développeur](https://yeuxdelibad.net/Programmation/tk-tools/owi)

Owi est un remplaçant à Wicd pour OpenBSD. Il permet de scanner les
réseaux Wifi disponibles pour se connecter à l'un deux en quelques
clics.

Il va créer une interface trunk0 si une connexion ethernet est
disponible pour basculer sur la connexion la plus efficace.


![aperçu owi](owi.png)


## Dépendances
Les bibliothèques tkinter pour python3 sont nécessaires : 

	pkg_add python-tkinter


## Utilisation
Il est nécessaire d'avoir les droits superutilisateur pour utiliser owi
: 

    doas owi


## Connexion automatique
owi peut automatiquement se connecter à un réseau wifi connu. Pour cela,
il faut le lancer avec l'option ``-d`` : 

    $ doas owi -d
    > Scanning WiFi networks
    > Found kamehameha in known AP
    ifconfig: SIOCSTRUNKPORT: Invalid argument

    ifconfig: SIOCSTRUNKPORT: Device busy

    > Checking if connexion ok
    > Connected :)
